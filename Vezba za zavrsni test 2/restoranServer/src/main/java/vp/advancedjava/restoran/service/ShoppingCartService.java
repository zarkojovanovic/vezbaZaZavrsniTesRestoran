package vp.advancedjava.restoran.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import vp.advancedjava.restoran.model.ShoppingCart;
import vp.advancedjava.restoran.repository.ShoppingCartRepository;

@Component
public class ShoppingCartService {
	
	@Autowired
	ShoppingCartRepository shoppingCartRepository;
	
	public ShoppingCart findByUsername(String username) {
		return shoppingCartRepository.findByUsername(username);
	}
	
	public ShoppingCart save(ShoppingCart shoppingCart) {
		return shoppingCartRepository.save(shoppingCart);
	}
}
