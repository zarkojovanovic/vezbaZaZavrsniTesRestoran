package vp.advancedjava.restoran.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Component;

import vp.advancedjava.restoran.model.Item;
import vp.advancedjava.restoran.repository.ItemRepository;

@Component
public class ItemService {
	
	@Autowired
	ItemRepository itemRepo;
	
	public Page<Item> findAll(Pageable page) {
		return itemRepo.findAll(page);
	}
	
	public Item findOne(Long id) {
		return itemRepo.findOne(id);
	}
	
	public Item save(Item item) {
		return itemRepo.save(item);
	}
	
	public void remove(Long id) {
		itemRepo.delete(id);
	}
	
	public Page<Item> findByName(String name, Pageable page) {
		return itemRepo.findByNameContains(name, page);
	}
}
