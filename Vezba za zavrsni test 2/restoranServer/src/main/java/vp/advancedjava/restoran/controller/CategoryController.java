package vp.advancedjava.restoran.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

import vp.advancedjava.restoran.model.Category;
import vp.advancedjava.restoran.service.CategoryService;


@RestController
public class CategoryController {
	
	@Autowired
	CategoryService categoryService;
	
	@GetMapping(value = "api/categories")
	public List<Category> getAll() {
		final List<Category> retVal = categoryService.findAll();
		
		return retVal;
	}
}
