package vp.advancedjava.restoran.controller;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import vp.advancedjava.restoran.model.Item;
import vp.advancedjava.restoran.model.ShoppingCart;
import vp.advancedjava.restoran.service.ShoppingCartService;

@RestController
public class ShoppingCartController {
	
	@Autowired
	ShoppingCartService shoppingCartService;
	
	@GetMapping(value = "api/shoppingCart")
	public List<Item> getShoppingCart() {
		Authentication auth = SecurityContextHolder.getContext().getAuthentication();
		ShoppingCart sc = shoppingCartService.findByUsername(auth.getName());
		return sc.getItems();
	}
	
	@PostMapping(value = "api/shoppingCart")
	public ShoppingCart addItem(@RequestBody Item item) {
		Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
		ShoppingCart sc = shoppingCartService.findByUsername(authentication.getName());
		if(sc==null) {
			sc = new ShoppingCart(authentication.getName(), new ArrayList<>());
		}
		
			sc.getItems().add(item);
			sc = shoppingCartService.save(sc);			
		return sc;
	}
	
	
}
