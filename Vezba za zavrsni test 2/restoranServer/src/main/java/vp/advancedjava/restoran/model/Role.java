package vp.advancedjava.restoran.model;


public enum Role {
    ADMINISTRATOR,
    WORKER
}
