package vp.advancedjava.restoran.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Component;

import vp.advancedjava.restoran.model.Category;
import vp.advancedjava.restoran.repository.CategoryRepository;

@Component
public class CategoryService {

	@Autowired
	CategoryRepository categoryRepo;
	
	public List<Category> findAll() {
		return categoryRepo.findAll();
	}
	public Page<Category> findAll(Pageable page) {
		return categoryRepo.findAll(page);
	}
	
	public Category findOne(Long id) {
		return categoryRepo.findOne(id);
	}
	
	public Category save(Category category) {
		return categoryRepo.save(category);
	}
	
	public void remove(Long id) {
		categoryRepo.delete(id);
	}
	
	
}
