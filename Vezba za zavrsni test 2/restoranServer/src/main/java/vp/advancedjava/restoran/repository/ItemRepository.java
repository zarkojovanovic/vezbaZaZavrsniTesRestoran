package vp.advancedjava.restoran.repository;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Component;

import vp.advancedjava.restoran.model.Item;

@Component
public interface ItemRepository extends JpaRepository<Item, Long> {

	public Page<Item> findByNameContains(String name, Pageable page);
}
