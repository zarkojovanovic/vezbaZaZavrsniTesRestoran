package vp.advancedjava.restoran.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import vp.advancedjava.restoran.model.Item;
import vp.advancedjava.restoran.service.ItemService;

@RestController
public class ItemController {
	
	@Autowired
	ItemService itemService;
	
	@GetMapping(value= "api/menu")
	public Page<Item> getAll(Pageable page) {
		final Page<Item> retVal = itemService.findAll(page);
		return retVal;
	}
	
	@GetMapping(value ="api/menu/{id}")
	public Item getOne(@PathVariable Long id) {
		final Item retVal = itemService.findOne(id);
		return retVal;
	}
	
	@PostMapping(value= "api/menu")
	public Item save(@RequestBody Item item) {
		final Item retVal = itemService.save(item);
		return retVal;
	}
	@SuppressWarnings("rawtypes")
	@DeleteMapping(value= "api/menu/{id}")
	public ResponseEntity delete(@PathVariable Long id) {
		if (itemService.findOne(id) != null) {
			itemService.remove(id);
			return new ResponseEntity<>(HttpStatus.OK);
		} else 
			return new ResponseEntity<>(HttpStatus.NOT_FOUND);
	}
	
	@GetMapping(value = "api/menu", params = "inputName")
	public Page<Item> getByName(@RequestParam String inputName, Pageable page) {
		final Page<Item> retVal = itemService.findByName(inputName, page);
		return retVal;
	}
}
