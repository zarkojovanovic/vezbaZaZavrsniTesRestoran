package vp.advancedjava.restoran.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Component;

import vp.advancedjava.restoran.model.Category;

@Component
public interface CategoryRepository extends JpaRepository<Category, Long> {

}
