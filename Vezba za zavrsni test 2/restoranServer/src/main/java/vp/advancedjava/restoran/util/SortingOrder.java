package vp.advancedjava.restoran.util;

public enum SortingOrder {
    ASC,
    DESC
}
