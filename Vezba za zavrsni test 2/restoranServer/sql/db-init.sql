
use  restoran;

insert into category(name) values
	('pizza'),
	('dessert'),
	('pasta'),
	('vine'),
	('coffee');
insert into item(name, category_id, price) values
	('Margarita', 1, 750.00),
	('Calzona', 1, 850.99),
	('Carbonara', 3, 455.55),
	('Shiraz', 4, 5552.52),
	('Vranac', 4, 2500.00),
	('Cake', 2, 522.00),
	('Tiramisu', 2, 254.57),
	('Espresso', 5, 52.5),
	('Capuccino', 5, 155.52),
	('Margarita', 1, 750.00),
	('Calzona', 1, 850.99),
	('Carbonara', 3, 455.55),
	('Shiraz', 4, 5552.52),
	('Vranac', 4, 2500.00),
	('Cake', 2, 522.00),
	('Tiramisu', 2, 254.57),
	('Espresso', 5, 52.5),
	('Capuccino', 5, 155.52),
	('Margarita', 1, 750.00),
	('Calzona', 1, 850.99),
	('Carbonara', 3, 455.55),
	('Shiraz', 4, 5552.52),
	('Vranac', 4, 2500.00),
	('Cake', 2, 522.00),
	('Tiramisu', 2, 254.57),
	('Espresso', 5, 52.5),
	('Capuccino', 5, 155.52);
	

-- insert users
-- password is 12345 (bcrypt encoded) 
insert into security_user (username, password, first_name, last_name, role) values 
	('admin', '$2a$04$4pqDFh9SxLAg/uSH59JCB.LwIS6QoAjM9qcE7H9e2drFuWhvTnDFi', 'Admin', 'Admin', 'ADMINISTRATOR');
-- password is abcdef (bcrypt encoded)
insert into security_user (username, password, first_name, last_name, role) values 
	('petar', '$2a$04$Yr3QD6lbcemnrRNLbUMLBez2oEK15pdacIgfkvymQ9oMhqsEE56EK', 'Petar', 'Petrovic', 'WORKER');
