import { Injectable } from '@angular/core';
import { Observable } from 'rxjs/Rx';
import { Category } from '../model/category-model';
import { HttpClient, HttpHeaders, HttpParams }  from '@angular/common/http';
@Injectable()
export class CategoryService {

  private readonly path: string = 'api/categories';

  constructor(private httpClient: HttpClient) { }

  getCategories() {
   return this.httpClient.get<Category[]>(this.path);
  }
}
