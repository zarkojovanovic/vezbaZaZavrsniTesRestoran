import { Injectable } from '@angular/core';
import { Item } from '../model/item-model';
import { Observable } from 'rxjs/Rx';
import { HttpHeaders } from '@angular/common/http';
import { HttpClient } from '@angular/common/http';
@Injectable()
export class OrderService {

  private readonly path = "api/shoppingCart"
  
    constructor(private httpClient: HttpClient) { }

  order(order: Item) {
  
    let headers = new HttpHeaders({'Content-Type': 'application/json'});
    return this.httpClient.post(this.path, order, {headers});
  }

  getCart() {
    return this.httpClient.get(this.path);
  }
  removeOrder(itemId: number) {
    let headers = new HttpHeaders({'Content-Type' : 'application/text'});
    return this.httpClient.delete(this.path+"/"+itemId, { responseType: 'text', headers });
  }
}
