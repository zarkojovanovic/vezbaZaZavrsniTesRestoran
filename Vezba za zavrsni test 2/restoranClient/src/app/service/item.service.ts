import { Injectable } from '@angular/core';
import { Observable } from 'rxjs/Rx';
import { HttpClient, HttpHeaders, HttpParams } from '@angular/common/http';
import { Item } from '../model/item-model';
@Injectable()
export class ItemService {

  private readonly path: string = 'api/menu';

  constructor(private httpClient: HttpClient) { }

  getItems(currentPage: number) {
    let params = new HttpParams();
    params = params.append('page' ,currentPage.toString());
    return this.httpClient.get<Item[]>(this.path, {params}); 

    
  }
  getOne(itemId: number) {
    return this.httpClient.get("api/menu"+"/" + itemId);
    
  }
  findByName(inputName: string, currentPage: number) {
    let params = new HttpParams();
    params = params.append('inputName', inputName);
    params = params.append('page' ,currentPage.toString());
    return this.httpClient.get<Item[]>(this.path, {params});
  }
  save(itemToAdd: Item) {
    let headers = new HttpHeaders({'Content-Type': 'application/json'});
    return this.httpClient.post(this.path, itemToAdd, {headers});
  }

  delete(itemId: number) {
    let headers = new HttpHeaders({'Content-Type': 'application/text'});
    return this.httpClient.delete(this.path+ "/" + itemId, { responseType: 'text', headers });
  }
}
