import { Category } from './category-model'; 

export class Item implements ItemInterface {
    public name: string;
    public category: Category;
    public price: number;

    constructor(itemCfg: ItemInterface) {
        this.name = itemCfg.name;
        this.category = itemCfg.category;
        this.price = itemCfg.price;
    }
}
interface ItemInterface {
    name: string;
    category: Category;
    price: number;
}