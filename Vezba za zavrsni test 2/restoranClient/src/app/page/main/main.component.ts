import { Component, OnInit, ViewEncapsulation } from '@angular/core';
import { ItemService } from '../../service/item.service';
import { CategoryService } from '../../service/category.service';
import { OrderService } from '../../service/order.service';
import { Item } from '../../model/item-model';
import { Category } from '../../model/category-model';
import { AuthenticationService } from '../../service/authentication-service.service';
@Component({
  selector: 'app-main',
  templateUrl: './main.component.html',
  styleUrls: ['./main.component.css'],
  encapsulation: ViewEncapsulation.None
})
export class MainComponent implements OnInit {
  itemToAdd: Item = {
    name: '',
    category: {
      name: ''
    },
    price: 0
  }
  orderItem: Item;
  itemsToOrder: Item[];
  inputName: ''
  categories: Category[];
  items: Item[];
  finalPrice: number=0;
  currentPage: number =0;
  totalPages: number;
  constructor(private itemService: ItemService,
              private authService: AuthenticationService,
              private categoryService: CategoryService,
              private orderService: OrderService) { }

  ngOnInit() {
    this.loadData();
    this.getCart();
  }

  loadData() {
    this.itemService.getItems(this.currentPage).subscribe(
      (resp: Item[])=> {
        this.items = resp['content'];
        this.totalPages = resp['totalPages'];
      }
    );
    this.categoryService.getCategories().subscribe(
      (resp: Category[]) =>{
        this.categories = resp;
      });
  }
  update(item: Item) {
    this.itemToAdd = item;
  }
  save() {
    this.itemService.save(this.itemToAdd).subscribe(
      (resp: Item) =>{
        this.loadData();
        this.reset();
       
      });
  }
  delete(itemId: number) {
    this.itemService.delete(itemId).subscribe(
      (resp: string) =>{
        this.loadData();
      });
  }
  findByName() {
    this.itemService.findByName(this.inputName, this.currentPage).subscribe(
      (resp: Item[]) => {
        this.items = resp['content'];
        this.totalPages = resp['totalPages'];
      });
  }
  removeOrder(orderId: number) {
    this.orderService.removeOrder(orderId).subscribe(
      (resp) => {
        this.getCart();
      });
  }

  order(orderItemId: number) {
    this.itemService.getOne(orderItemId).subscribe(
      (resp: Item) => {
        this.orderItem = resp;
    this.orderService.order(this.orderItem).subscribe(
      (resp: Item[]) => {
       this.itemsToOrder = resp['items'];
        for(var i=0; i < this.itemsToOrder.length; i++) {
          this.finalPrice += this.itemsToOrder[i].price;
        }
      });
      });
  }
  getCart() {
    this.orderService.getCart().subscribe(
      (resp: Item[]) => {
        this.itemsToOrder = resp['items'];
       
      });
  }
 
  changePage(i:number){
    this.currentPage+=i;
    this.loadData();
  }
   isLoggedIn() {
    return this.authService.isLoggedIn();
  }
  getTableClass() {
    if(this.isLoggedIn()) {
      return "col-sm-8";
    } else {
      return "col-sm-12";
    }
  }
  reset() {
    this.itemToAdd ={
      name: '',
      category: {
        name: ''
      },
      price: 0
    }
  }

}
